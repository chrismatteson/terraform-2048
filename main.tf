provider "aws" {
  region = "us-west-2"
}

terraform {
  backend "s3" {
    bucket  = "gloom-terraform"
    key     = "aws/2048/gloom.tfstate"
    encrypt = true
    region  = "us-west-2"
  }
}

data "terraform_remote_state" "vpc" {
  backend = "s3"

  config {
    bucket = "${lookup(var.remote_state_bucket, terraform.workspace)}"
    key    = "${lookup(var.remote_state_vpc_key, terraform.workspace)}"
    region = "${lookup(var.remote_state_region, terraform.workspace)}"
  }
}


module "2048_demo" {
  source          = "github.com/gabcoyne/terraform_modules/2048"
  # source = "../terraform-modules/2048"
  region          = "us-west-2"
  logging_enabled = "false"
}
